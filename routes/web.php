<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home',[PageController::class,'index']);
Route::get('/about',[AboutController::class,'index']);
Route::get('/users',[UserController::class,'index'])->name('user.index');
Route::get('/user/register',[UserController::class,'create']);
Route::delete('/test/{id}',[UserController::class,'destroy']);
Route::post('/users',[UserController::class,'store']);
Route::get('/user/{user}',[UserController::class,'show']);



