## Laravel - InertiaJS

**1. Install laravel**
```
composer create-project laravel/laravel example-app
```
### server side
**2. Install inertiaJs**
```
composer require inertiajs/inertia-laravel
```
**3. Tambahkan file app.blade.php pada resources\views**
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet" />
    <script src="{{ mix('/js/app.js') }}" defer></script>
  </head>
  <body>
    @inertia
  </body>
</html>
```
**4. Install middleware**
```
php artisan inertia:middleware
```
dan pasangkan pada kernel > App\Http\Kernel
```php
'web' => [
    // ...
    \App\Http\Middleware\HandleInertiaRequests::class,
],
```
### client side
**5. Install dependency**
```
npm init
```
```
npm install vue
```

```
# Vue 2
npm install @inertiajs/inertia @inertiajs/inertia-vue
yarn add @inertiajs/inertia @inertiajs/inertia-vue
```
**6. Initialize app, buat file app.js di > resources\js**
```javascript

require('./bootstrap');

import { App, plugin } from '@inertiajs/inertia-vue'
import Vue from 'vue'

Vue.use(plugin)

const el = document.getElementById('app')

new Vue({
  render: h => h(App, {
    props: {
      initialPage: JSON.parse(el.dataset.page),
      resolveComponent: name => require(`./Pages/${name}`).default,
      //resolveComponent: name => import(`./Pages/${name}`).then(module => module.default),
    },
  }),
}).$mount(el)

```

**7. Problem solving**
*file vue tidak dikenali oleh laravel mix, maka tambahkan atau gantikan code berikut Menambahkan .vue() :

```javascript
// You can simply use this
mix.js('resources/js/app.js', 'public/js')
.vue()
.postCss('resources/css/app.css', 'public/css', [
        //
]);
```

ref :
https://stackoverflow.com/questions/65607153/how-to-fix-the-error-you-may-need-an-appropriate-loader-to-handle-this-file-typ

### Mulai Coding 
**8.Inersia links**

```javascript
<inertia-link href="/logout" method="post" as="button" type="button">Logout</inertia-link>

// Renders as:
<button type="button">Logout</button>
```

ref : https://inertiajs.com/links

**9. Layout**
